import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {Observable} from "rxjs";
//import {map} from "rxjs/operators";
//import {user} from "./user";

interface jsTech {
  value: string;
  viewValue: string;
}

interface jsAngular {
  value: string;
  viewValue: string;
}

interface jsReact {
  value: string;
  viewValue: string;
}

interface jsVue {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  title = 'fe-form';
  completedForm!: FormGroup;
  selectedJSTech!: string;

  ngOnInit() {
    this.completedForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      dateOfBirth: new FormControl('', Validators.required),
      framework: new FormControl('', Validators.required),
      frameworkVersion: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email], this.uniqueEmail.bind(this))
    });
  }

  uniqueEmail(control: AbstractControl): Observable<ValidationErrors | null> {
    return this.completedForm.value/*this.user.validateMail(control.value)
      .pipe(map((res: string | null) => {
          if (res !== null) {
            return {
              invalidEmail: true
            };
          }
          return null;
        })
      );*/
  };


  jsTechs: jsTech [] = [
    {value: 'angular', viewValue: 'Angular'},
    {value: 'react', viewValue: 'React'},
    {value: 'vue', viewValue: 'Vue'}
  ];

  jsAngulars: jsAngular [] = [
    {value: '1.1.1', viewValue: '1.1.1'},
    {value: '1.2.1', viewValue: '1.2.1'},
    {value: '1.3.3', viewValue: '1.3.3'}
  ];

  jsReacts: jsReact [] = [
    {value: '2.1.2', viewValue: '2.1.2'},
    {value: '3.2.4', viewValue: '3.2.4'},
    {value: '4.3.1', viewValue: '4.3.1'}
  ];

  jsVues: jsVue [] = [
    {value: '3.3.1', viewValue: '3.3.1'},
    {value: '5.2.1', viewValue: '5.2.1'},
    {value: '5.1.3', viewValue: '5.1.3'}
  ];

  onSubmit(): void {
    console.warn('Ваши данные переданы', this.completedForm.value);
  }
}
